﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebProje2.Startup))]
namespace WebProje2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
